<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 07/01/2017
 * Time: 02:16
 */

namespace vr\upload\image;

use ReflectionException;
use vr\upload\AttributeDescriptor;
use vr\upload\connectors\DataConnector;
use vr\upload\image\filters\ResizeFilter;
use vr\upload\image\placeholders\Placeholder;
use vr\upload\Mediator;
use vr\upload\sources\UriSource;
use Yii;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class ImageAttributeDescriptor
 * @package vr\upload
 * @property array $urlSet
 * @property Model $model
 */
class ImageAttributeDescriptor extends AttributeDescriptor
{
    /**
     *
     */
    const DEFAULT_IMAGE_DIMENSION = 320;

    /**
     * @var
     */
    public $placeholder;

    /**
     * @var array
     */
    public $thumbnails = [];

    /**
     * @var Mediator
     */
    private $_mediator;

    /**
     * @return mixed|string
     * @throws ReflectionException
     * @throws InvalidConfigException
     */
    public function getUrlSet()
    {
        $url = parent::getUrl();

        if (!$this->placeholder && empty($url)) {
            return null;
        }

        if ($this->placeholder) {
            $url = $this->applyPlaceholder($url);
        }

        $set = [
            'original' => $url,
        ];

        $connector = $this->getConnector();

        // Adding thumbnails
        foreach ($this->thumbnails as $name => $dimension) {

            if (!filter_var($this->model->{$this->attribute}, FILTER_VALIDATE_URL)) {

                $filename = $this->getThumbnailFilename($name);
                $url      = $connector->url($filename);

                if ($this->placeholder) {
                    $url = $this->applyPlaceholder($url, $dimension);
                }
            }

            $set[$name] = $url;
        }

        return $set;
    }

    /**
     * @param $url
     * @param $dimension
     *
     * @return mixed
     * @throws InvalidConfigException
     */
    public function applyPlaceholder($url, $dimension = null)
    {
        /** @var Placeholder $placeholder */
        $placeholder = Yii::createObject($this->placeholder);

        if (($placeholder->useWhen & Placeholder::USE_IF_NULL) && empty($url)) {

            list($width, $height) = $this->parseDimension($dimension);

            return $placeholder->getImageUrl($width, $height);
        }

        if (($placeholder->useWhen & Placeholder::USE_IF_MISSING) && !$this->isUrlValid($url)) {
            list($width, $height) = $this->parseDimension($dimension);

            return $placeholder->getImageUrl($width, $height);
        }

        return $url;
    }

    /**
     * @param $dimension
     *
     * @return array
     * @throws InvalidConfigException
     */
    private function parseDimension($dimension)
    {
        list($width, $height) = Utils::parseDimension($dimension);

        if (!$width || !$height) {
            /** @var ResizeFilter $filter */
            $filter = $this->findResizeFilter();

            if ($filter) {
                list($width, $height) = Utils::parseDimension($filter->dimension);
            }
        }

        if (!$width || !$height) {
            $width = $height = self::DEFAULT_IMAGE_DIMENSION;
        }

        return [$width, $height];
    }

    /**
     * @return null|object
     * @throws InvalidConfigException
     */
    private function findResizeFilter()
    {
        foreach ($this->filters as $filter) {
            $instance = Yii::createObject($filter);
            if (is_a($instance, ResizeFilter::class)) {
                return $instance;
            }
        }

        return null;
    }

    /**
     * @param $url
     *
     * @return bool
     */
    private function isUrlValid($url)
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            return false;
        }

        $headers = get_headers($url);

        return strpos($headers[0], '200') !== false;
    }

    /**
     * @param $name
     *
     * @return string
     */
    private function getThumbnailFilename($name)
    {
        $basename = $this->model->{$this->attribute};

        if (empty($basename)) {
            return null;
        }

        $info = pathinfo($basename);

        $filename  = ArrayHelper::getValue($info, 'filename');
        $extension = ArrayHelper::getValue($info, 'extension');

        return "{$filename}-{$name}.{$extension}";
    }

    /**
     * @return mixed|string
     * @throws ReflectionException
     * @throws InvalidConfigException
     */
    public function getUrl()
    {
        $url = parent::getUrl();

        if ($this->placeholder) {
            $url = $this->applyPlaceholder($url);
        }

        return $url;
    }

    /**
     * @param Mediator $mediator
     *
     * @return bool
     * @throws ReflectionException
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function upload(Mediator $mediator)
    {
        $this->_mediator = $mediator;

        return parent::upload($mediator);
    }

    /**
     * @throws ReflectionException
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function onAfterInsert()
    {
        parent::onAfterInsert();

        $this->generateThumbnails();
    }

    /**
     * @return bool
     * @throws InvalidConfigException
     * @throws ReflectionException
     * @throws NotFoundHttpException
     */
    public function generateThumbnails()
    {
        $filename = $this->model->{$this->attribute};

        if (empty($filename)) {
            return false;
        }

        /** @var DataConnector $connector */
        $connector = $this->getConnector();

        if (!$this->_mediator) {
            $this->_mediator = (new UriSource([
                'uri' => $connector->locate($filename, true),
            ]))->createMediator();
        }

        foreach ($this->thumbnails as $name => $dimension) {
            $thumbnailFilename = $this->getThumbnailFilename($name);

            (new ResizeFilter([
                'dimension' => $dimension,
                'crop'      => false,
            ]))->apply($this->_mediator);

            $connector->upload($this->_mediator, $thumbnailFilename);
        }

        $this->_mediator->cleanUp();

        return true;
    }

    /**
     * @throws ReflectionException
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function onAfterUpdate()
    {
        parent::onAfterUpdate();

        $this->generateThumbnails();
    }

    /**
     * @param $filename
     *
     * @return bool
     * @throws ReflectionException
     * @throws InvalidConfigException
     */
    protected function validateFile($filename)
    {
        if (!parent::validateFile($filename)) {
            return false;
        }

        if (false === getimagesize($this->getConnector()->locate($filename))) {

            $this->model->addError($this->attribute, "File is not a proper image file");
            $this->model->{$this->attribute} = null;

            return false;
        }

        return true;
    }
}