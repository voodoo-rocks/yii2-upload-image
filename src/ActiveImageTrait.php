<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 07/01/2017
 * Time: 01:30
 */

namespace vr\upload\image;

use vr\upload\sources\BinarySource;
use yii\base\Component;

/**
 * Class ActiveImageTrait
 * @package vr\upload
 */
trait ActiveImageTrait
{
    /**
     * @param string $attribute
     *
     * @return mixed|null|string
     */
    public function url($attribute)
    {
        return $this->getBehaviour()->url($attribute);
    }

    /**
     * @return ImageBehavior
     */
    private function getBehaviour()
    {
        /** @var Component $component */
        $component = $this;

        $component->ensureBehaviors();

        foreach ($component->behaviors as $behavior) {
            if (is_a($behavior, ImageBehavior::class)) {
                return $behavior;
            }
        }

        return null;
    }

    /**
     * @param bool $simulate
     *
     * @return bool
     * @throws \ReflectionException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function repair($simulate = false)
    {
        return $this->getBehaviour()->repair($simulate);
    }

    /**
     * @param string $attribute
     *
     * @return mixed|null|string
     */
    public function urlSet($attribute)
    {
        return $this->getBehaviour()->urlSet($attribute);
    }

    /**
     * @return mixed
     */
    public function cleanUp()
    {
        return $this->getBehaviour()->cleanUp();
    }

    /**
     * @return mixed
     */
    public function drop(string $attribute)
    {
        return $this->getBehaviour()->drop($attribute);
    }

    /**
     * @param string       $attribute
     * @param BinarySource $source
     * @param array        $options
     *  Following options are supported:
     *  - defaultExtension. If set and the web service cannot determine the extension automatically based on the
     *  binary content this extension will be used. Otherwise it will be ignored. It can be used for some content
     *  files which are not described properly in the web service configuration files.
     *
     * @return bool
     * @throws \ReflectionException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function upload($attribute, $source, $options = [])
    {
        return $this->getBehaviour()->upload($attribute, $source, $options);
    }
}