<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 07/01/2017
 * Time: 01:47
 */

namespace vr\upload\image;

use vr\upload\BinaryBehavior;

/**
 * Class ImageBehavior
 * @package vr\upload\image
 * Usage:
 *  1. Add this to your model class
 *  public function behaviors()
 *  {
 *      return [
 *          [
 *              'class' => ImageBehavior::className(),
 *              'binaryAttributes' => [
 *                  'image' => [
 *                      'template' => '{base}-{datetime}-{tag}',
 *                      // 'template' => '{attribute:title}-{base}-{datetime}-{tag}',
 *                      'connector' => [
 *                          'class' => FileSystemDataConnector::className(),
 *                      ],
 *                      'placeholder' => [
 *                          'class' => PlaceBear::className()
 *                      ],
 *                      'filters' => [
 *                          'resize' => [
 *                              'class' => ResizeFilter::className(),
 *                              'dimension' => [100, 200]
 *                          ],
 *                      ],
 *                      'thumbnails' => [
 *                          'xs' => [120, 120]
 *                      ]
 *                  ]
 *              ],
 *          ],
 *      ];
 *  }
 *  2. Don't forget to add ActiveImageTrait to your class to define missing functions
 *  3. Add this code to the model where you upload your image
 *      if (($instance = UploadedFile::getInstance($this, 'image'))) {
 *          $product->upload('image', new UploadedFileSource([
 *              'uploaded' => $instance
 *          ]));
 *      }
 */
class ImageBehavior extends BinaryBehavior
{
    /**
     * @var string
     */
    public $descriptorClass = ImageAttributeDescriptor::class;

    /**
     * Returns the qualified URI of the binary
     *
     * @param      $attribute
     *
     * @return array URI of the binary
     */
    public function urlSet($attribute)
    {
        /** @var ImageAttributeDescriptor $descriptor */
        $descriptor = $this->getDescriptor($attribute);

        return $descriptor->urlSet;
    }

    public function generateThumbnails($attribute)
    {
        /** @var ImageAttributeDescriptor $descriptor */
        $descriptor = $this->getDescriptor($attribute);
        return $descriptor->generateThumbnails($attribute);
    }
}