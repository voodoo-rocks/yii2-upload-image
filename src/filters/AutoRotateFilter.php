<?php


namespace vr\upload\image\filters;


use Imagick;
use Imagine\Image\ImageInterface;
use Throwable;
use vr\core\ArrayHelper;
use vr\upload\filters\Filter;
use vr\upload\Mediator;
use yii\imagine\Image;

/**
 * Class AutoRotateFilter
 * @package vr\upload\image\filters
 */
class AutoRotateFilter extends Filter
{
    /**
     *
     */
    const ROTATION_MAP = [
        1 => 0,
        8 => 270,
        3 => 180,
        6 => 90,
    ];

    public function init()
    {
        // pixel cache max size
        Imagick::setResourceLimit(Imagick::RESOURCETYPE_MEMORY, 256);

        // maximum amount of memory map to allocate for the pixel cache
        Imagick::setResourceLimit(Imagick::RESOURCETYPE_MAP, 256);

        Image::$driver = [Image::DRIVER_GD2];
    }

    /**
     * @param Mediator $mediator
     *
     * @return mixed
     */
    public function apply($mediator)
    {
        try {
            $data        = exif_read_data($mediator->filename);
            $orientation = ArrayHelper::getValue($data, 'Orientation');

            $angle = ArrayHelper::getValue(self::ROTATION_MAP, $orientation, 0);

            if ($angle) {
                /** @var ImageInterface $imagine */
                $imagine = Image::getImagine()->open($mediator->getFilename());

                $imagine->rotate($angle);
                $imagine->save($mediator->filename);
            }
        } catch (Throwable $throwable) {
            return false;
        }
        
        return true;
    }

}