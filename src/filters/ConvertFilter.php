<?php


namespace vr\upload\image\filters;

use Imagick;
use Throwable;
use vr\upload\filters\Filter;
use vr\upload\Mediator;
use yii\imagine\Image;

/**
 * Class ConvertFilter
 * @package vr\upload\image\filters
 */
class ConvertFilter extends Filter
{
    public $format;

    public function init()
    {
        // pixel cache max size
        Imagick::setResourceLimit(Imagick::RESOURCETYPE_MEMORY, 256);

        // maximum amount of memory map to allocate for the pixel cache
        Imagick::setResourceLimit(Imagick::RESOURCETYPE_MAP, 256);

        Image::$driver = [Image::DRIVER_GD2];
    }

    /**
     * @param Mediator $mediator
     * @return bool|void
     */
    public function apply($mediator)
    {
        try {
            if (pathinfo($mediator->filename, PATHINFO_EXTENSION) == $this->format) {
                return true;
            }

            $imagine = Image::getImagine()->open($mediator->filename);

            $dir      = pathinfo($mediator->filename, PATHINFO_DIRNAME);
            $filename = pathinfo($mediator->filename, PATHINFO_FILENAME);

            $newFilename = "{$dir}/{$filename}.{$this->format}";
            $imagine->save($newFilename);

            unlink($mediator->filename);

            $mediator->filename  = $newFilename;
            $mediator->extension = $this->format;

            return true;
        } catch (Throwable $throwable) {

        }

        return false;
    }
}