<?php

namespace vr\upload\image\filters;

use Imagick;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\Point;
use Throwable;
use vr\upload\filters\Filter;
use vr\upload\image\Utils;
use vr\upload\Mediator;
use yii\imagine\Image;

/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 07/01/2017
 * Time: 02:40
 */
class ResizeFilter extends Filter
{
    /**
     * @var
     */
    public $dimension;

    /**
     * @var bool
     */
    public $crop = true;

    /**
     * @param Mediator $mediator
     *
     * @return mixed
     */
    public function apply($mediator)
    {
        try {
            // pixel cache max size
            Imagick::setResourceLimit(Imagick::RESOURCETYPE_MEMORY, 256);

            // maximum amount of memory map to allocate for the pixel cache
            Imagick::setResourceLimit(Imagick::RESOURCETYPE_MAP, 256);

            /** @var ImageInterface $imagine */
            Image::$driver = [Image::DRIVER_GD2];
            $imagine       = Image::getImagine()->open($mediator->getFilename());

            $this->performResize($imagine, $this->dimension, $this->crop);
            $imagine->save($mediator->filename);

            return true;
        } catch (Throwable $throwable) {

        }

        return false;
    }

    /**
     * @param ImageInterface $imagine
     * @param                $resize
     * @param bool $crop
     *
     * @return mixed
     */
    private function performResize($imagine, $resize, $crop = false)
    {
        $box = $imagine->getSize();

        list($width, $height) = Utils::parseDimension($resize);

        $box = $box->scale(max($width / $box->getWidth(), $height / $box->getHeight()));
        $imagine->resize($box);

        if ($crop) {
            $point = new Point(($box->getWidth() - $width) / 2,
                ($box->getHeight() - $height) / 2);

            $imagine->crop($point, new Box($width, $height));
        }

        return $box;
    }
}