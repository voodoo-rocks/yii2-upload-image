<?php


namespace vr\upload\image\filters;

use vr\upload\filters\Filter;
use vr\upload\Mediator;

/**
 * Class HeicToJpegFilter
 * @package vr\upload\image\filters
 */
class HeicToJpegFilter extends Filter
{
    const JPEG_FORMAT = 'jpg';

    const HEIC_FORMAT = 'heic';

    /**
     * @param Mediator $mediator
     * @return bool|void
     */
    public function apply($mediator)
    {
        if ($mediator->extension !== self::HEIC_FORMAT || !file_exists($mediator->filename)) {
            return false;
        }

        $dir      = pathinfo($mediator->filename, PATHINFO_DIRNAME);
        $filename = pathinfo($mediator->filename, PATHINFO_FILENAME);

        $newFilename = "{$dir}/{$filename}." . self::JPEG_FORMAT;
        exec("heif-convert {$mediator->filename} {$newFilename}");

        if (file_exists($newFilename)) {
            unlink($mediator->filename);

            $mediator->filename  = $newFilename;
            $mediator->extension = self::JPEG_FORMAT;
        }

        return true;
    }
}